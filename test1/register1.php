<?php
session_start();

if(isset($_SESSION['usr_id'])) {
	header("Location: index.php");
}

include_once 'dbconnect.php';

//set validation error flag as false
$error = false;

//check if form is submitted
if (isset($_POST['signup'])) {
	include_once 'phpmailer/PHPMailerAutoload.php';
	include_once 'utility.php';
	$name = $_POST['name'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$cpassword = $_POST['cpassword'];

	//name can contain only alpha characters and space
	if (!preg_match("/^[a-zA-Z ]+$/",$name)) {
		$error = true;
		$name_error = "Name must contain only alphabets and space";
	}
	if(!filter_var($email,FILTER_VALIDATE_EMAIL)) {
		$error = true;
		$email_error = "Please Enter Valid Email ID";
	}
	if(strlen($password) < 6) {
		$error = true;
		$password_error = "Password must be minimum of 6 characters";
	}
	if($password != $cpassword) {
		$error = true;
		$cpassword_error = "Password and Confirm Password doesn't match";
	}
	if (!$error) {
		$hash = md5( rand(0,1000) );

		$sqlQuery = "INSERT INTO registered_users(username,password,email,hash,active) VALUES(" .
		"'" . $name . "'" . ","
		. "'" .  md5($password) . "'". ","
		. "'" .$email . "'". ","
		. "'" .$hash . "'". ","
		. 0 . ")" ;
		echo "$sqlQuery";
		if(mysqli_query($con, $sqlQuery)) {
			$to=$email;
			$message = 'Welcome to voise <br />Please click the link below to verify and activate your account.http://www.voise.com/activate.php?key='.$hash."&email=".$email;
				$body=$message;
				$mailInfo = array('to' =>$to, 'subject' =>'Welcome to Voise',  'name' => '','message' =>'', 'body' =>$body, 'altBody' =>'','attach'=>'');
				$mail=sendMail($mailInfo);
				$successmsg = "Successfully Registered! <a href='login.php'>Click here to Login</a>";
			} else {
				$errormsg = "Error in registering...Please try again later!";
			}
		}
	}
	?>

	<!DOCTYPE html>
	<html>
	<head>
		<title>VOISE LOGIN</title>
		<meta content="width=device-width, initial-scale=1.0" name="viewport" >
		<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
		<link rel="stylesheet" href="css/signup.css" type="text/css" />
	</head>
	<body class="bg-img">

		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- <div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">VOISE</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar1">
		<ul class="nav navbar-nav navbar-right">
		<li class="active"><a href="login.php">Login</a></li>
		<li><a href="register.php">Sign Up</a></li>
	</ul>
</div> -->
</div>
</nav>

<div class="container">
	<div class="row">
		<div class="col-md-12 text-center loginLogo">
			<a href="/">
				<img src="img/voise_logo.png">
			</a>
		</div>
		<div class="col-md-4 col-md-offset-4 well">
			<div class="col-md-12 text-center loginDiv">
				<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="signupform">
					<fieldset>
						<legend>Sign Up</legend>

						<div class="form-group">
							<input type="text" name="name" placeholder="User Name" required value="<?php if($error) echo $name; ?>" class="form-control loginInput"/>
							<span class="text-danger"><?php if (isset($name_error)) echo $name_error; ?></span>
						</div>

						<div class="form-group">
							<input type="text" name="email" placeholder="Email" required value="<?php if($error) echo $email; ?>" class="form-control loginInput"/>
							<span class="text-danger"><?php if (isset($email_error)) echo $email_error; ?></span>
						</div>

						<div class="form-group">
							<input type="password" name="password" placeholder="Password" required class="form-control loginInput"/>
							<span class="text-danger"><?php if (isset($password_error)) echo $password_error; ?></span>
						</div>

						<div class="form-group">
							<input type="password" name="cpassword" placeholder="Confirm Password" required class="form-control loginInput"/>
							<span class="text-danger"><?php if (isset($cpassword_error)) echo $cpassword_error; ?></span>
						</div>
						<label class="checkbox-inline">
							<input type="checkbox" value="">I agree to the  <a href="#">Voise.com</a> Terms of Service
						</label>
						<div class="form-group">
							<input type="submit" name="signup" value="Sign Up" class="btn btn-primary loginBtn"/>
						</div>
					</fieldset>
				</form>
				<span class="text-success"><?php if (isset($successmsg)) { echo $successmsg; } ?></span>
				<span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>
			</div>
		</div>
	</div>
	<div class="row loginFtr">
		<div class="col-md-4 col-md-offset-4 text-center">
			Already Registered? <a href="login.php">Login Here</a>
		</div>
	</div>
</div>
<script src="js/jquery-1.10.2.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
