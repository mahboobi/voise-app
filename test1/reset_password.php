<?php
	if(isset($_POST["reset-password"])) {
		$conn =mysqli_connect("voisecomdddb.mysql.db", "voisecomdddb", "Unique123", "voisecomdddb");
		$sql =  "UPDATE registered_users SET password = '" . md5($_POST["password"]). "' WHERE username = '" . $_POST["username"] . "'";
		$result = mysqli_query($conn,$sql);
		//echo $sql;
		$success_message = "Password is reset successfully.";
		
	}
?>

<!DOCTYPE html>

<html>

<head>

	<title>VOISE LOGIN</title>

	<meta content="width=device-width, initial-scale=1.0" name="viewport" >

	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />

	<link rel="stylesheet" href="css/signup.css" type="text/css" />
	<link rel="icon" type="image/png" href="http://voise.com/test1/img/voise_fav.png" />
	<script>
function validate_password_reset() {
	if((document.getElementById("password").value == "") && (document.getElementById("confirm_password").value == "")) {
		document.getElementById("validation-message").innerHTML = "Please enter new password!"
		return false;
	}
	if(document.getElementById("password").value  != document.getElementById("confirm_password").value) {
		document.getElementById("validation-message").innerHTML = "Both password should be same!"
		return false;
	}
	
	return true;
}
</script>

</head>

<body class="bg-img">


	<div class="container">

		<div class="row">

			<div class="col-md-12 text-center loginLogo">

					<a href="/">

			           <img src="img/voise_logo.png">

			        </a>

				</div>

			<div class="col-md-4 col-md-offset-4 well">

				

				<div class="col-md-12 text-center loginDiv">

					<form name="frmReset" id="frmReset" method="post" onSubmit="return validate_password_reset();">
						<fieldset>

							<legend>Forgot Password</legend>
							
							<?php if(!empty($success_message)) { ?>
								<div class="success_message"><?php echo $success_message; ?></div>
							<?php } ?>

							<div id="validation-message">
								<?php if(!empty($error_message)) { ?>
								<?php echo $error_message; ?>
								<?php } ?>
							</div>

							<div class="form-group">
								<div class="text-left colorWhite"><label for="Password">Password</label></div>
								<div><input type="password" name="password" id="password" class="form-control loginInput"></div>

							</div>

							<div class="form-group">
								<div class="text-left colorWhite"><label for="email">Confirm Password</label></div>
								<div><input type="password" name="confirm_password" id="confirm_password" class="form-control loginInput"></div>

							</div>
							<input type="hidden" name="username"  value="<?php echo $_GET["name"] ?>" class="form-control loginInput">
							<div class="form-group">

								<input type="submit" name="reset-password" id="reset-password" value="Reset Password" class="btn btn-primary loginBtn"/>

							</div>

						</fieldset>
					</form>
				
				</div>

			</div>

		</div>

	</div>



<script src="js/jquery-1.10.2.js"></script>

<script src="js/bootstrap.min.js"></script>

<!-- <script type="text/javascript" src="js/login.js"></script> -->

</body>

</html>