<?php

session_start();



if(isset($_SESSION['usr_id'])!="") {

	header("Location: platform.php");

}



include_once 'dbconnect.php';



//check if form is submitted



if (isset($_POST['login'])) {



	$email = mysqli_real_escape_string($con, $_POST['email']);

	$password = mysqli_real_escape_string($con, $_POST['password']);

	$result = mysqli_query($con, "SELECT * FROM registered_users WHERE email = '" . $email. "' and password = '" . md5($password) . "' and active=1");



	if ($row = mysqli_fetch_array($result)) {

		$_SESSION['usr_id'] = $row['id'];

		$_SESSION['usr_name'] = $row['username'];

		header("Location: platform.php");

	} else {

		$errormsg = "Incorrect Email or Password!!!";

	}

}

?>



<!DOCTYPE html>

<html>

<head>

	<title>VOISE LOGIN</title>

	<meta content="width=device-width, initial-scale=1.0" name="viewport" >

	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />

	<link rel="stylesheet" href="css/signup.css" type="text/css" />
	<link rel="icon" type="image/png" href="http://voise.com/test1/img/voise_fav.png" />

</head>

<body class="bg-img">


<div class="container">

	<div class="row">

		<div class="col-md-12 text-center loginLogo">

				<a href="/">

		           <img src="img/voise_logo.png">

		        </a>

			</div>

		<div class="col-md-4 col-md-offset-4 well">

			

			<div class="col-md-12 text-center loginDiv">

				<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="loginform">

					<fieldset>

						<legend>Login</legend>

						

						<div class="form-group">							

							<input type="text" name="email" placeholder="Email" required class="form-control loginInput" />

						</div>



						<div class="form-group">

							<input type="password" name="password" placeholder="Password" required class="form-control loginInput" />

						</div>



						

						<label class="checkbox-inline">

					      	<input type="checkbox" value="">Remember Me?

					    </label>

					    <div class="forgotpassword">

							<a href="forgotpassword.php">Forgot Password</a>

						</div>

					    <div class="form-group">

							<input type="submit" name="login" value="Login" class="btn btn-primary loginBtn" />

						</div>

					</fieldset>

				</form>



				<span class="text-danger"><?php if (isset($errormsg)) { echo $errormsg; } ?></span>

			</div>

		</div>

	</div>

	<div class="row loginFtr">

		<div class="col-md-4 col-md-offset-4 text-center">	

		New User? <a href="register.php">Sign Up Here</a>

		</div>

	</div>

</div>



<script src="js/jquery-1.10.2.js"></script>

<script src="js/bootstrap.min.js"></script>

<!-- <script type="text/javascript" src="js/login.js"></script> -->

</body>

</html>

