<?php
if(!class_exists('PHPMailer')) {
    require('phpmailer/class.phpmailer.php');
	require('phpmailer/class.smtp.php');
}

require_once("mail_configuration.php");

$mail = new PHPMailer();

//$emailBody = "<div>" . $user["username"] . ",<br><br><p>Click this link to recover your password<br><a href='" . PROJECT_HOME . "reset_password.php?name=" . $user["username"] . "'>" . PROJECT_HOME . "reset_password.php?name=" . $user["username"] . "</a><br><br></p>Regards,<br> Admin.</div>";
$emailBody = file_get_contents('email.html');
$emailBody = str_replace('{{link}}', PROJECT_HOME . "reset_password.php?name=" . $user["username"] ."", $emailBody);
// $emailBody = str_replace('{{url}}',	PROJECT_HOME, $emailBody);
$emailBody = str_replace('{{user-name}}', $user["username"], $emailBody);

$mail->IsSMTP();
$mail->SMTPDebug = 0;
$mail->SMTPAuth = TRUE;
$mail->SMTPSecure = "tls";
$mail->Port     = 587;
$mail->Username = 'admin@voise.com';    					// SMTP username chandra.sekhar@formaccorp.com
$mail->Password = 'admin@123';
$mail->Host     = "SSL0.OVH.NET";
$mail->Mailer   = MAILER;

$mail->SetFrom('admin@voise.com','admin');
$mail->AddReplyTo('admin@voise.com','voise');
$mail->ReturnPath='admin@voise.com';
$mail->AddAddress($user["email"]);
$mail->Subject = "Forgot Password Recovery";
$mail->MsgHTML($emailBody);
$mail->IsHTML(true);

if(!$mail->Send()) {
	$error_message = 'Problem in Sending Password Recovery Email';
} else {
	$success_message = 'Please check your email to reset password!';
}

?>
