<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>VOISE</title>

  <meta name="description">

  <meta name="keywords">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <![endif]-->



  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

  <link rel="stylesheet" type="text/css" href="css/animate.css">

  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="icon" type="image/png" href="/img/voise_fav.png" />

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<div class="sub-header">
  <a href="/tokenswap"><i class="fa fa-exchange" aria-hidden="true"></i> VOISE TOKEN SWAP <i class="fa fa-exchange" aria-hidden="true"></i></a>
</div>
  <div class="aboutHeader">

    <!--  <div class="bg-color"> -->
    <header id="main-header">





      <!-- Modal -->

      <div id="myModal" class="modal fade" role="dialog">

        <div class="modal-dialog">



          <!-- Modal content-->

          <div class="modal-content">

            <div class="modal-header">

              <button type="button" class="close menuCLs" data-dismiss="modal">&times;</button>

            </div>

            <div class="modal-body">

              <div class=" navbar-collapse">

                <ul class="nav navbar-nav navbar-right navbar-border mblMenu">

                  <li><a href="/" >Home</a></li>

                  <li><a href="token" >Token</a></li>

                  <li><a href="https://medium.com/@voiseit" >Blog</a></li>

                  <li class="support">

                    <a href="" data-toggle="modal" data-target="#support">Support<span class="supportHeader"></span></a>

                  </li>

                  <li><a href="/login" >Login</a></li>

                  <li><a href="/register"  >SignUp</a></li>

                </ul>

              </div>

            </div>

          </div>



        </div>

      </div>

      <nav class="navbar navbar-default ">

        <div class="container">

          <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="modal" data-target="#myModal" data-backdrop="false">

              <span class="icon-bar"></span>

              <span class="icon-bar"></span>

              <span class="icon-bar"></span>

            </button>

            <a class="navbar-brand aboutBrand aboutHeaderbrand"  href="/">

              <img src="img/voise_logo.png">

              <!--    <span>VOISE</span> -->

            </a>

          </div>

          <div class="collapse navbar-collapse aboutMenu aboutHeadermenu" id="lauraMenu">

            <ul class="nav navbar-nav navbar-right navbar-border">

              <li ><a href="/">Home</a></li>

              <li><a href="token" >Token</a></li>

              <li><a href="https://medium.com/@voiseit" >Blog</a></li>

              <li class="support">

                <a href="" data-toggle="modal" data-target="#support">Support<span class="supportHeader"></span></a>

              </li>

              <li><a href="/login" >Login</a></li>

              <li><a href="/register"  >SignUp</a></li>

            </ul>

          </div>

        </div>
        
      </nav>

    </header>

  </div>
