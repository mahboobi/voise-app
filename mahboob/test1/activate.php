<?php
include_once 'dbconnect.php';
$activationmessage = "";
if(isset($_GET['email']) && !empty($_GET['email']) && isset($_GET['key']) && !empty($_GET['key'])){
	$email = $_GET['email']; // Set email variable
	$hash = $_GET['key']; // Set hash variable
	$search = mysqli_query($con,"SELECT email, hash, active FROM registered_users WHERE email='".$email."' AND hash='".$hash."' AND active='0'");
	$match  = mysqli_num_rows($search);
	if($match > 0){
		// We have a match, activate the account
		$sqlQuery = "UPDATE registered_users SET active='1' WHERE email='".$email."' AND hash='".$hash."' AND active='0'";
		if(mysqli_query($con, $sqlQuery)) {
			$activationmessage = 'Your account has been activated, you can now login.';
		}else{
			$activationmessage  = 'Something went wrong! Try later';
		}
	}else{
		$activationmessage = "Either your account is already active <br> or its an invalid url.";
	}

}else{
	$activationmessage =  "Not this time my friend, You have wrong url";
}

?>


<!DOCTYPE html>
<html>
<head>
	<title>VOISE LOGIN</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" >
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="css/signup.css" type="text/css" />
</head>
<body class="bg-img">

	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">

		</div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center loginLogo">
				<a href="/">
					<img src="img/voise_logo.png">
				</a>
			</div>
			<div class="col-md-4 col-md-offset-4 well">
				<div class="col-md-12 text-center loginDiv" style="  color: white;  font-weight: bold;">
					<?php echo $activationmessage; ?>
				</div>
			</div>
		</div>
		<div class="row loginFtr">
			<div class="col-md-4 col-md-offset-4 text-center">
				<a href="login.php">Login Here</a>
			</div>
		</div>
	</div>
	<script src="js/jquery-1.10.2.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
