<?php
	if(!empty($_POST["forgot-password"])){
		$conn = mysqli_connect("voisecomdddb.mysql.db", "voisecomdddb", "Unique123", "voisecomdddb");
		
		$condition = "";
		if(!empty($_POST["user-login-name"])) 
			$condition = " username = '" . $_POST["user-login-name"] . "'";
		if(!empty($_POST["user-email"])) {
			if(!empty($condition)) {
				$condition = " and ";
			}
			$condition = " email = '" . $_POST["user-email"] . "'";
		}
		
		if(!empty($condition)) {
			$condition = " where " . $condition;
		}

		$sql = "Select * from registered_users " . $condition;
		$result = mysqli_query($conn,$sql);
		$user = mysqli_fetch_array($result);
		
		if(!empty($user)) {
			require_once("forgot-password-recovery-mail.php");
		} else {
			$error_message = 'No User Found';
		}
	}
?>


<!DOCTYPE html>

<html>

<head>

	<title>VOISE LOGIN</title>

	<meta content="width=device-width, initial-scale=1.0" name="viewport" >

	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />

	<link rel="stylesheet" href="css/signup.css" type="text/css" />
	<link rel="icon" type="image/png" href="http://voise.com/test1/img/voise_fav.png" />
	<script>
function validate_forgot() {
	if((document.getElementById("user-login-name").value == "") && (document.getElementById("user-email").value == "")) {
		document.getElementById("validation-message").innerHTML = "Login name or Email is required!"
		return false;
	}
	return true
}
</script>

</head>

<body class="bg-img">


	<div class="container">

		<div class="row">

			<div class="col-md-12 text-center loginLogo">

					<a href="/">

			           <img src="img/voise_logo.png">

			        </a>

				</div>

			<div class="col-md-4 col-md-offset-4 well">

				

				<div class="col-md-12 text-center loginDiv">
					<form name="frmForgot" id="frmForgot" method="post" onSubmit="return validate_forgot();">

						<fieldset>

						<legend>Forgot Password</legend>

						<?php if(!empty($success_message)) { ?>
							<div class="success_message"><?php echo $success_message; ?></div>
						<?php } ?>

						<div id="validation-message">
							<?php if(!empty($error_message)) { ?>
							<?php echo $error_message; ?>
							<?php } ?>
						</div>

						<div class="form-group">							


							<div class="text-left colorWhite"><label for="username">Username</label></div>
							<div><input type="text" name="user-login-name" id="user-login-name" class="form-control loginInput"></div>

						</div>

						<div class="form-group colorWhite">OR</div>

						<div class="form-group">

							<div class="text-left colorWhite"><label for="email">Email</label></div>
							<div><input type="text" name="user-email" id="user-email" class="form-control loginInput"></div>

						</div>					

						

					    <div class="form-group">

							<input type="submit" name="forgot-password" id="forgot-password" class="btn btn-primary loginBtn" value="Submit"  />

						</div>

					</fieldset>


<!-- 
					<h1>Forgot Password?</h1>
						

						<div class="form-group">
							<div><label for="username">Username</label></div>
							<div><input type="text" name="user-login-name" id="user-login-name" class="input-field loginInput"> Or</div>
						</div>
						
						<div class="form-group">
							<div><label for="email">Email</label></div>
							<div><input type="text" name="user-email" id="user-email" class="input-field loginInput"></div>
						</div>
						
						<div class="field-group">
							<div><input type="submit" name="forgot-password" id="forgot-password" value="Submit" class="form-submit-button loginBtn"></div>
						</div> -->	
					</form>
				</div>

			</div>

		</div>

	</div>



<script src="js/jquery-1.10.2.js"></script>

<script src="js/bootstrap.min.js"></script>

<!-- <script type="text/javascript" src="js/login.js"></script> -->

</body>

</html>

