<?php

session_start();

include_once 'dbconnect.php';

?>

<!DOCTYPE html>

<html>

<head>

	<title>VOISE Music Streaming</title>

	<meta content="width=device-width, initial-scale=1.0" name="viewport" >

	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
	<link rel="icon" type="image/png" href="/img/voise_fav.png" />
	<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107746786-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107746786-1');
</script>


</head>

<body>



<nav class="navbar navbar-default" role="navigation">

	<div class="container-fluid">

		<div class="navbar-header">

			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">

				<span class="sr-only">Toggle navigation</span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

			</button>

			<a class="navbar-brand" href="index.php">VOISE</a>

		</div>

		<div class="collapse navbar-collapse" id="navbar1">

			<ul class="nav navbar-nav navbar-right">

				<?php if (isset($_SESSION['usr_id'])) { ?>

				<li><p class="navbar-text">Signed in as <?php echo $_SESSION['usr_name']; ?></p></li>

				<li><a href="logout.php">Log Out</a></li>

				<?php } else { ?>

				<li><a href="login">Login</a></li>

				<li><a href="register">Sign Up</a></li>

				<?php } ?>

			</ul>

		</div>

	</div>

</nav>



<script src="js/jquery-1.10.2.js"></script>

<script src="js/bootstrap.min.js"></script>

</body>

</html>
