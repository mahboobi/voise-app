<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

  <title>VOISE: TOKENSWAP</title>

  <meta name="description">

  <meta name="keywords">
  <!-- Global Site Tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107746786-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-107746786-1');
  </script>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <![endif]-->
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,900|Noto+Sans:400,700" rel="stylesheet">
<link type="text/css" href="./jquery.mmenu/jquery.mmenu.all.css" rel="stylesheet" />

  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">

  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

  <link rel="stylesheet" type="text/css" href="css/style.css">


  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="icon" href="img/favicon.ico" type="image/x-icon">

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<div class="page">

<div class="sub-header">
  <a href="/tokenswap"><i class="fa fa-exchange" aria-hidden="true"></i> VOISE TOKEN SWAP <i class="fa fa-exchange" aria-hidden="true"></i></a>
</div>



    <header id="main-header">
      <div class="container">

      <a class="navbar-brand logo" href="/">
            <img src="img/voise_single.png" alt='voise-logo'> VOISE
      </a>
        <nav class="navbar ">


                    <ul class="">

                        <li class="active"><a href="/">Home</a></li>

                        <li><a href="token">Token</a></li>

                        <li><a href=" https://medium.com/@voisecom">Blog</a></li>

                        <li class="support">
                            <a href="" data-toggle="modal" data-target="#support">Support<span class="supportHeader"></span></a>
                        </li>

                    </ul>

        </nav>
        <div class="menu-right pull-right">
          <a href="#" class="btn btn-transparent">Login</a>
          <a href="#" class="btn btn-red">SignUp</a>
        </div>
        <a href="#menu" class="menu-dashboard"><i class="fa fa-bars" aria-hidden="true"></i></a>
      </div>

    </header>

<section class="header-1">

</section>


  <!-- <section class="sub-page-content">

    <div class="container">

      <div class="row">



        <div class="col-md-12 no-padding">

          <div class=" aboutTxt">


            <div class="col-md-8 col-md-offset-2">
              VSM is the currency of the VOISE platform, they are used to purchase content. Built around ethereum's smart contract technology, the user is free to store the tokens on his personal desktop wallet,any web based wallet like myetherwallet or the VOISE platform to speed up the process of purchasing content like explained above in the platform section. You can trade the Voisiums like you would with any other cryptocurrency.
            </div>

          </div>
        </div>
      </div>
      </div>
  </section> -->




  <section class="token-info">
    <div class="container">
      <div class="row">

      <div class="col-md-12 aboutBody">
          <h3 class="tokenSwapTitle">TOKEN SWAP VSM to VOISE </h3>
          <div class="col-md-12 no-padding aboutDiv tokenDesc">
            Convert your VSM tokens to VOISE tokens.
          </div>
          <div class="col-md-12 no-padding aboutDiv tokenDiv" align="center">

            <div class="row address-row">
              <div class="col-md-2">
                VSM Token Address
              </div>
              <div class="col-md-10">

              <h1 class="tokenAddress"> 0x83eEA00D838f92dEC4D1475697B9f4D3537b56E3 </h1>

            </div>
            </div>
<h4 style=" font-weight: 900;"> Send your VSM tokens to this address </h4>
            <div class="tokenExtraText">
              Do not send tokens from exchanges,you will lose your tokens.<br>
              Send tokens from Myetherwallet,Parity or Mist.<br>
               Every 30 minutes we will distribute the tx received.
              </div>
              <p>
              Token swap rate : 1 VSM <i class="fa fa-exchange" aria-hidden="true"></i> 1000 VOISE | <a href="https://medium.com/@voisecom/announcing-vsm-token-swap-a-small-effort-towards-bettering-voise-platform-51559b183dcf"><b>See New token conversion guide.</b> </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>




  <footer class="footer-copy ">
    <div class="container">
      <div class="copyright">© 2017 VOISE-All Rights Reserved</div>
    </div>
  </footer>


  <script src="js/jquery.min.js"></script>

  <script src="js/bootstrap.min.js"></script>

  <script type="text/javascript" src="./jquery.mmenu/jquery.mmenu.all.js"></script>

  <script type="text/javascript">
    $(document).ready(function($) {
              $("#menu").mmenu();
          });
      </script>
      <script>
      var stickyOffset = $('#main-header').offset().top;

    $(window).scroll(function(){
    var sticky = $('#main-header'),
    scroll = $(window).scrollTop();

    if (scroll >= stickyOffset) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
    });
  </script>

  <script>
$(document).ready(function() {
    function checkWidth() {
        var windowSize = $(window).width();

        if (windowSize <= 479) {
            console.log("screen width is less than 480");
        }
        else if (windowSize <= 719) {
            console.log("screen width is less than 720 but greater than or equal to 480");
        }
        else if (windowSize <= 959) {
            console.log("screen width is less than 960 but greater than or equal to 720");
        }
        else if (windowSize >= 960) {
          $('.multi-item-carousel').carousel({
            interval: false
          });
          $('.multi-item-carousel .item').each(function(){
            var next = $(this).next();
            if (!next.length) {
              next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            if (next.next().length>0) {
              next.next().children(':first-child').clone().appendTo($(this));
            } else {
             $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            }
          });
        }
    }

    // Execute on load
    checkWidth();
    // Bind event listener
    $(window).resize(checkWidth);
})
  </script>
  <div id="support" class="modal fade" role="dialog">

      <div class="modal-dialog">



          <!-- Modal content-->

          <div class="modal-content">

              <div class="modal-header text-center">

                  <button type="button" class="close" data-dismiss="modal">&times;</button>

                  <h4 class="modal-title">Support</h4>

              </div>

              <div class="modal-body text-center support">

                  <p class="lh-30 supportLi">If you have any questions,please contact with our support</p>

                  <p class="lh-30">Support : <a href="mailto:business@voise.com">support@voise.com</a></p>

                  <p class="lh-30">Bussiness : <a href="">business@voise.com</a></p>

                  <p class="lh-30">Telegram : <a href="https://t.me/voisecom">@voisecom</a></p>

                  <p class="lh-30">Slack : <a href="https://voise.herokuapp.com/">VOISE</a></p>

              </div>

          </div>

      </div>

  </div>
</div>

<nav id="menu">
    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about">About us</a>
            <ul>
                <li><a href="/about/history">About Voise</a></li>
                <li><a href="/about/team">The team</a></li>


            </ul>
        </li>
        <li><a href="/token">Token</a></li>
        <li><a href="#" >Login</a></li>
        <li><a href="#">SignUp</a></li>
        <li><a href="mailto:info@voise.com">Contact</a></li>
    </ul>
</nav>


</body>
