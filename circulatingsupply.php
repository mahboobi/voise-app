<!DOCTYPE html>
<html>
<title>Voise</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>

  <!--
  This is a *MASSIVE* CPU drain and just a bit of fun.
  I wouldn't use it on anything serious, but see it in action here: http://beeroclock.in
-->

<script>
$.getJSON('https://api.ethplorer.io/getAddressInfo/0xe009c117f0745edc4c2bf8812df259a43a69512d?apiKey=freekey', function(data) {
  var non_swapped = data.tokens[0].balance;
  var total = data.tokens[0].tokenInfo.totalSupply;
  var circulating_supply = (total - non_swapped)/100000000;
  $("#name-container").text(circulating_supply);
});
</script>
<div class="bubbles">
  <div id="name-container">
    Loading...
  </div>

</div>

</body>
</html>
