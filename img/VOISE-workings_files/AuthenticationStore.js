var AuthenticationStore = function () {

    var _this = this;  // Capture scope
    var authenticated = false;

    this.isAuthenticated = function () {
        return authenticated;
    };

    var _signedIn = function () {
        authenticated = true;
        _this.emit();
    };

    var _signedOut = function () {
        var sessionCookie = App.Util.getCookie('session');
        if (sessionCookie) {
            _serverLogout(sessionCookie);
        }
        App.Util.removeCookie('session');
        App.Util.removeCookie('mixPanelId');
        App.Util.removeCookie('menucookie');
        authenticated = false;
        _this.emit();
    };

    var _userContextUpdate = function (userContext) {
        if (userContext.userOid) {
            _signedIn();
        }
    };

    this.dispatchToken = App.dispatcher.register(function (action, payload) {
        if (action === App.Actions.SIGNED_IN) {
            _signedIn();
        } else if (action === App.Actions.SIGNED_OUT) {
            _signedOut();
        } else if (action === App.Actions.USER_CONTEXT_UPDATE) {
            _userContextUpdate(payload)
        }
    }.bind(this));

    var _serverLogout = function (sessionToken) {
        App.Util.ajax({
            url: "/rest/auth/logout",
            data: {
                "sessionToken": sessionToken
            },
            dataType: 'json'
        });
    }
};


AuthenticationStore.prototype = new AbstractStore();
