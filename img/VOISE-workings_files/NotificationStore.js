var NotificationStore = function () {

    var notifications = [];

    var _this = this;  // Capture scope

    var _createNotification = function(msg, type, owner){
        // give the new notification an id
        var id = App.Util.getUUID();

        if (type === "network") {
            // if notification is of type "network" filter out all older notifications of type network
            notifications = notifications.filter(function(item){
               return (item.type !== "network")
            });
        }
        // add  new notification
        notifications.push({"id": id, "msg": msg, "type": type, "owner": owner});
        _this.emit();
    };

    var _removeNotification = function(id){
        notifications = notifications.filter(function(item){
           return (item.id !== id)
        });
        _this.emit();
    };

    var _removeOwnerNotifications = function(owner){
        notifications = notifications.filter(function(item){
            return (item.owner !== owner)
        });
        _this.emit();
    };

    this.getNotifications = function(){
        return notifications;
    };

    this.dispatchToken = App.dispatcher.register(function(action, payload) {
        if (action === App.Actions.CREATE_NOTIFICATION) {
            // For now all notifications are of type "network" which means they don't stack one on another in the view
            // Once you get a new one you clear the old ones of same type, we might want to play with this functionality later...
            _createNotification(payload.msg, "network", payload.owner);
        }
        else if (action === App.Actions.REMOVE_NOTIFICATION){
            _removeNotification(payload.id)
        }else if (action === App.Actions.CLEAR_OWNER_NOTIFICATIONS){
            _removeOwnerNotifications(payload.owner)
        }
    });

};

NotificationStore.prototype = new AbstractStore();
