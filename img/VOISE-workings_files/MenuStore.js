var MenuStore = function () {

    this.getMenu = function (authenticated) {
        return {
            items: [
                {
                    id: 'links',
                    title: "STORAGE",
                    url: '/links',
                    newTab: false
                },
                {
                    id: 'features',
                    title: "FEATURES",
                    url: '/features',
                    newTab: false
                },
                {
                    id: 'upgrade',
                    title: "UPGRADE NOW",
                    url: '/upgrade',
                    newTab: false
                },
                {
                    id: 'usageguide',
                    title: "USAGE GUIDE",
                    url: 'https://docs.odrive.com',
                    newTab: false,
                    newSection: true
                },
                {
                    id: 'forum',
                    title: "USER FORUM",
                    url: 'https://forum.odrive.com',
                    newTab: false
                },
                {
                    id: 'blog',
                    title: "BLOG",
                    url: 'http://medium.odrive.com',
                    newTab: false
                },
                {
                    id: 'customerservice',
                    title: "CUSTOMER SERVICE",
                    url: '/customerservice',
                    newTab: false
                },
                {
                    id: 'resources',
                    title: "RESOURCES",
                    menuParent: true,
                    showChildren: false,
                    childMenu: [
                        {
                            id: 'faq',
                            title: "FAQ",
                            url: '/faq',
                            newTab: false
                        },
                        {
                            id: 'terms',
                            title: "TERMS",
                            url: 'https://docs.odrive.com/docs/odrive-terms-of-service',
                            newTab: false
                        },
                        {
                            id: 'privacypolicy',
                            title: "PRIVACY POLICY",
                            url: 'https://docs.odrive.com/docs/odrive-terms-of-service#section--15-privacy-policy-',
                            newTab: false
                        },
                        {
                            id: 'facebook',
                            title: "Facebook",
                            url: 'http://www.facebook.com/odrivesync',
                            newTab: true
                        },
                        {
                            id: 'twitter',
                            title: "Twitter",
                            url: 'http://www.twitter.com/odrive',
                            newTab: true
                        }
                    ]
                },
                {
                    id: 'aboutus',
                    title: "ABOUT",
                    url: '/about',
                    newTab: false
                }
            ]
        };
    }
};

MenuStore.prototype = new AbstractStore();
