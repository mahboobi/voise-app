var EventRegisterStore = function () {

    // trackId is unique for each web app instance. It will belong to the signed in user.
    var trackId = null;

    var _sendEventTrack = function (payload) {
/*
            payload should be formatted to the entire json data payload :

            data: {type: "visit",
                   track: App.stores.eventRegisterStore.getTrackId(),
                   props: '{' +
                   ' "referrer":"google",' +
                   ' "referrer-url":"",' +
                   ' "campaign-id":"",' +
                   ' "ad-id":"",' +
                   ' "landing-id":0 }'},
*/
        App.Util.ajax({
            url: "/event/register",
            data: payload,
            dataType: 'json',
            success: function (response) {
                // Nothing required to do. There is a response.data = {'status': 'success'} return if desired.
            }.bind(this),
            error: function (response) {
            }.bind(this)
        });
    };

    this.getTrackId = function () {
        // Note : the id is set to the existing cookie. If there is not a cookie, then one is created.
        if (!EventRegisterStore.trackId) {
            EventRegisterStore.trackId = App.Util.getCookie("trackId");
            if (!EventRegisterStore.trackId) {
                this.setTrackId(App.Util.getUUID());
            }
        }
        return EventRegisterStore.trackId;
    };

    this.setTrackId = function (usrTrackId) {
        EventRegisterStore.trackId = null;
        App.Util.removeCookie("trackId");
        App.Util.setCookie("trackId", usrTrackId);
        EventRegisterStore.trackId = App.Util.getCookie("trackId");
    };

    this.initialize = function () {
        this.getTrackId();
    };

    this.dispatchToken = App.dispatcher.register(function(action, payload) {
        if (action === App.Actions.EVENT_TRACK) {
            _sendEventTrack(payload);
        }
    });
};

EventRegisterStore.prototype = new AbstractStore();
