(function () {

    App = App || {};

    App.Util = {
        ajax: function (settings) {
            var error = settings.error;
            settings.error = function (response) {
                var jsonResponse = response.responseJSON;
                var errorCode;
                var messageCode;
                if (!jsonResponse) {
                    // Got no response back from the server
                    // alert("response: " + response.responseText + " status: " + response.status + " statusText: " + response.statusText);
                    errorCode = App.Errors.NETWORK_FAULT;
                    messageCode = 'NETWORK_FAULT';
                } else {
                    errorCode = jsonResponse.errorCode || App.Errors.UNKNOWN_ERROR;
                    messageCode = jsonResponse.messageCode;
                    // app layer handling for invalid session coming back from the server
                    if (errorCode === 104) {
                        messageCode = 'INVALID_ODRIVE_SESSION';
                        // tell authentication store to sign out user
                        App.dispatcher.dispatch(App.Actions.SIGNED_OUT)
                    }

                }
                if (error) {
                    error(errorCode, messageCode)
                }
            };
            return $.ajax(settings);
        },
        getUUID: function () {
            return (
                'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                    return v.toString(16);
                })
            );
        },
        getVersion: function () {
            if (window.location.href.match(/^https:\/\/dev/) || window.location.href.match(/^http:\/\/localhost/)) {
                return "dev";
            } else if (window.location.href.match(/^https:\/\/beta/)) {
                return "beta";
            } else {
                return "prod";
            }
        },
        formatDateString: function (unixEpochTime) {
            // Set the display time to required format.
            var nearestMidnight = new Date();
            nearestMidnight.setHours(0, 0, 0, 0);
            var nearestMidnightSeconds = nearestMidnight.getTime() / 1000;

            var daySeconds = 60 * 60 * 24;  // 24 hours
            var yesterdayMidnightSeconds = nearestMidnightSeconds - daySeconds;

            // Convert the file's seconds since epoch (unixEpoch) to string.
            var fileDatestring = new Date(unixEpochTime * 1000);

            // Format the time and date string to spec : 11/21/15 8:53 AM
            var timePeriod = "AM";
            var timeHours = fileDatestring.getHours();
            if (timeHours > 12) {
                timeHours -= 12;
                timePeriod = "PM";
            }
            var timeMinutes = fileDatestring.getMinutes();
            if (timeMinutes < 10) {
                timeMinutes = "0" + timeMinutes;
            }
            var fileLocalTime = timeHours + ":" + timeMinutes + " " + timePeriod;

            // Mac will get the "November 21, 2015" formatting from getLocalDate(), so just normalize here to "11/21/2015".
            var fileLocalDate = (fileDatestring.getMonth() + 1) + "/" + fileDatestring.getDate() + "/" + fileDatestring.getFullYear();

            return {timeString: fileLocalTime, dateString: fileLocalDate}
        },
        getBrowserData: function (nav) {
            var data = {};
            var ua = data.uaString = nav.userAgent;
            var browserMatch = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
            if (browserMatch[1]) {
                browserMatch[1] = browserMatch[1].toLowerCase();
            }

            var operaMatch = browserMatch[1] === 'chrome';
            if (operaMatch) {
                operaMatch = ua.match(/\bOPR\/([\d\.]+)/);
            }

            if (/trident/i.test(browserMatch[1])) {
                var msieMatch = /\brv[ :]+([\d\.]+)/g.exec(ua) || [];
                data.name = 'msie';
                data.version = msieMatch[1];
            }
            else if (operaMatch) {
                data.name = 'opera';
                data.version = operaMatch[1];
            }
            else if (browserMatch[1] === 'safari') {
                var safariVersionMatch = ua.match(/version\/([\d\.]+)/i);
                data.name = 'safari';
                data.version = safariVersionMatch[1];
            }
            else {
                data.name = browserMatch[1];
                data.version = browserMatch[2];
            }

            var versionParts = [];
            if (data.version) {
                var versionPartsMatch = data.version.match(/(\d+)/g) || [];
                for (var i = 0; i < versionPartsMatch.length; i++) {
                    versionParts.push(versionPartsMatch[i]);
                }
                if (versionParts.length > 0) {
                    data.majorVersion = versionParts[0];
                }
            }

            data.name = data.name || '(unknown browser name)';
            data.version = {
                full: data.version || '(unknown full browser version)',
                parts: versionParts,
                major: versionParts.length > 0 ? versionParts[0] : '(unknown major browser version)'
            };
            return data;
        },
        getCookie: function(cname) {
            var rv = docCookies.getItem(cname);
            return rv;
        },
        setCookie: function(cname, value) {
            var expire = false;
            var domain = App.cookieDomain;
            var rv = docCookies.setItem(cname,value,expire,'/',domain);
            return rv;
        },
        removeCookie: function(cname) {
            return docCookies.removeItem(cname, '/');   // returns true or false
        },
        getUrlParams: function() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function (m, key, value) {
                    vars[key] = value;
                });
            return vars;
        }
    };

    App.Constants = {
        GALLERY_IMAGE_DEFAULT_WIDTH: 200,
        GALLERY_IMAGE_DEFAULT_HEIGHT: 200,
        GALLERY_TARGET_ROW_HEIGHT:200,
        WEBLISTVIEW_STANDARD_WIDTH: 1300,
        WEBLISTVIEW_MINIMUM_WIDTH_PADDING: 35,
        WEBLISTVIEW_TYPECOLUMN_DROP_WIDTH: 820,
        WEBLISTVIEW_MODCOLUMN_DROP_WIDTH: 600,
        WEBROOTVIEW_TYPECOLUMN_DROP_WIDTH: 600
    };

    App.cookieDomain = '';

    App.dispatcher = new Dispatcher();

    App.Actions = {
        SIGNED_IN: 1,
        SIGNED_OUT: 2,
        REMOTE_UPDATE_FOLDER: 3,
        GET_MORE_FOR_FOLDER: 4,
        CREATE_NOTIFICATION: 5,
        REMOVE_NOTIFICATION: 6,
        CLEAR_OWNER_NOTIFICATIONS: 7,
        MIXPANEL_TRACK: 8,
        MIXPANEL_WTF: 9,
        MIXPANEL_SET_USER_PROFILE: 10,
        USER_CONTEXT_UPDATE: 11,
        TRACK_PAGE_VIEW: 12,
        EVENT_TRACK: 13,
        SET_MENU_COLOR: 14,
        REMOTE_UPDATE_WEBLINK_FOLDER: 15,
        GET_MORE_FOR_WEBLINK_FOLDER: 16,
        SET_WEBLINK_PASSWORD: 17
    };

    App.Errors = {
        INVALID_LINK_URI: 202,
        AUTHENTICATION_REQUIRED: 411,
        INVALID_SESSION: 104,
        NETWORK_FAULT: "NETWORK_FAULT",
        UNKNOWN_ERROR: "UNKNOWN_ERROR"
    };

    App.stores = {};
    App.stores.authenticationStore = new AuthenticationStore();
    App.stores.menuStore = new MenuStore();
    App.stores.fileStore = new FileStore();
    App.stores.weblinkStore = new WeblinkStore();
    App.stores.browserPropertyStore = new BrowserPropertyStore();
    App.stores.notificationStore = new NotificationStore();
    App.stores.mixpanelStore = new MixpanelStore();
    App.stores.googleAnalyticsStore = new GoogleAnalyticsStore();
    App.stores.eventRegisterStore = new EventRegisterStore();

    App.stringTable = new StringTable();

    App.Util.browserData = App.Util.getBrowserData(window.navigator || navigator);

    // Allow stores to initialize
    App.stores.mixpanelStore.initialize();

}).call(this);



// Add panel listener registration function to App.Util

(function (window) {
    // 4-tuples, [element, nextCallback, previousCallback, changesPreventedTimeout]
    var wheelListeners = [];

    // 3-tuple, [element, nextCallback, previousCallback]
    var keyListeners = [];

    var direction;

    var isDescendant = function (child, parent) {
        var node = child;
        while (node) {
            if (node === parent) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    };

    var preventYScrolling = function(event) {
        if (!event.deltaX) {
            event.preventDefault();
        }
    };

    var normalizeDelta = function() {
      // Keep a distribution of observed values, and scale by the
      // 33rd percentile.
      var distribution = [], done = null, scale = 30;
      return function(n) {
        // Zeroes don't count.
        if (n == 0) return n;
        // After 500 samples, we stop sampling and keep current factor.
        if (done != null) return n * done;
        var abs = Math.abs(n);
        // Insert value (sorted in ascending order).
        outer: do { // Just used for break goto
          for (var i = 0; i < distribution.length; ++i) {
            if (abs <= distribution[i]) {
              distribution.splice(i, 0, abs);
              break outer;
            }
          }
          distribution.push(abs);
        } while (false);
        // Factor is scale divided by 33rd percentile.
        var factor = scale / distribution[Math.floor(distribution.length / 3)];
        if (distribution.length == 500) done = factor;
        return n * factor;
      };
    }();

    window.addEventListener('keyup', function (originalEvent) {
        var key = originalEvent.keyCode ? originalEvent.keyCode : originalEvent.which;
        if (([33, 34, 38, 40].indexOf(key) < 0)) {
            return;  // We only are interested in up, down, pageUp and pageDown key presses
        }
        keyListeners.forEach(function (changeListener) {
            var element = changeListener[0];
            var callback = [34, 40].indexOf(key)>-1 ? changeListener[1] : changeListener[2];
            callback(originalEvent);
            originalEvent.preventDefault();
        });
    });


    var activeTouch = null;
    var startY;
    var swipeProcessed = false;

    window.addEventListener('touchstart', function(originalEvent) {
        // When the user starts touching an active panel, store the touch so we can ignore multitouch.
        if (!activeTouch) {
            wheelListeners.forEach(function (wheelListener) {
                var element = wheelListener[0];
                if (isDescendant(originalEvent.changedTouches[0].target, element)) {
                    activeTouch = originalEvent.changedTouches[0];
                    startY = activeTouch.pageY;
                    swipeProcessed = false;
                }
            });
        }
    });

    window.addEventListener('touchmove', function (originalEvent) {
        var movedTouch = null;
        var deltaY;

        if (!activeTouch) {
            return;
        }

        // Make sure the active touch is in the list of touches that moved.
        for (var i = 0; i < originalEvent.changedTouches.length; i++) {
            if (originalEvent.changedTouches[i].identifier === activeTouch.identifier) {
                movedTouch = originalEvent.changedTouches[i];
            }
        }
        // If their touch swiped more than 30px up or down, transition to the appropriate panel
        if (movedTouch) {
            if (!swipeProcessed) {
                deltaY = movedTouch.pageY - startY;
                if (Math.abs(deltaY) > 30) {
                    wheelListeners.forEach(function (wheelListener) {
                        var element = wheelListener[0];
                        var callback = deltaY < 0 ? wheelListener[1] : wheelListener[2];
                        if (isDescendant(activeTouch.target, element)) {
                            swipeProcessed = true;
                            callback();
                        }
                    });
                }
            }
            originalEvent.preventDefault();
        }
    });

    window.addEventListener('touchend', function(originalEvent) {
        // If the active touch ended, remove our reference to it.
        if (!activeTouch) {
            return;
        }
        for (var i = 0; i < originalEvent.changedTouches.length; i++) {
            if (originalEvent.changedTouches[i].identifier === activeTouch.identifier) {
                activeTouch = null;
                break;
            }
        }
    });

    window.addWheelListener(window, function (originalEvent) {
        var currentDirection = (originalEvent.detail<0 || originalEvent.deltaY>0) ? 1 : -1;
        normalizedDeltaY = Math.round(normalizeDelta(originalEvent.deltaY));
        if (Math.abs(normalizedDeltaY) < 30) {
            preventYScrolling(originalEvent);
            return;
        }

        wheelListeners.forEach(function (changeListener) {
            var element = changeListener[0],
                callback = originalEvent.deltaY > 0 ? changeListener[1] : changeListener[2];
                changesPrevented = changeListener[3];
            if (isDescendant(originalEvent.target, element)) {
                preventYScrolling(originalEvent);
                if(changesPrevented){
                    if (currentDirection !== direction) {
                        // If event is a change of direction, don't prevent it
                        callback(originalEvent);
                        // reset the timeout
                        clearTimeout(changeListener[3]);
                        changeListener[3] = setTimeout(function () {
                            changeListener[3] = null;
                        }, 700);
                    }
                }
                else {
                    // Call the callback, then prevent it from being called again for 700ms.
                    callback(originalEvent);
                    changeListener[3] = setTimeout(function () {
                        changeListener[3] = null;
                    }, 700);
                }
            }
        });
        direction = currentDirection;
    });

    App.Util.addPanelChangeListener = function(element, next, previous) {
        // Adds a listener for scroll events, and arrow keys.
        // Calls next() or previous() according to the user's navigation intent.
        wheelListeners.push([element, next, previous, null]);
        keyListeners.push([element, next, previous]);
    };

    App.Util.removePanelChangeListener = function(element) {
        // Removes listeners on the provided element.
        var i;
        for (i = 0; i < wheelListeners.length; i++) {
            if (wheelListeners[i][0] === element) {
                wheelListeners.splice(i, 1);
                i--;
            }
        }
        for (i = 0; i < keyListeners.length; i++) {
            if (keyListeners[i][0] === element) {
                keyListeners.splice(i, 1);
                i--;
            }
        }
    };

/*
 http://stackoverflow.com/questions/951791/javascript-global-error-handling

 When does the window.onerror Event Fire?

 In a nutshell, the event is raised when either 1.) there is an uncaught exception or 2.) a compile time error occurs.

 uncaught exceptions
 •throw "some messages"
 •call_something_undefined();
 •cross_origin_iframe.contentWindow.document;, a security exception

 compile error
 •<script>{</script>
 •<script>for(;)</script>
 •<script>"oops</script>
 •setTimeout("{", 10);, it will attempt to compile the first argument as a script

 Browsers supporting window.onerror
 •Chrome 13+
 •Firefox 6.0+
 •Internet Explorer 5.5+
 •Opera 11.60+
 •Safari 5.1+

 // Alternative to wrapping addEventListener and catching exceptions:
 https://bugsnag.com/blog/js-stacktraces/
*/
    // Create an exception for testing.
    App.Util.createException = function() {
        try {
            this.undef();
        } catch (e) {
            if (e.stack) {
                console.log(e.stack);
            }
            throw e;
        }
    };

    window.onerror = function(msg, url, line, col, error) {
        // Note that col & error are new to the HTML 5 spec and may not be supported in every browser.
        var colValue = !col ? '' : col;
        var errorValue = !error ? '' : error;
        var stackValue = !error ? '' : !error.stack ? "[No Stack]" : error.stack;

        ///////////////////////////////////
        // TODO
        // IE9 <, mobile IE
        //var column = column || (window.event && window.event.errorCharacter);
        //var stack = [];
        //var f = arguments.callee.caller;
        //while (f) {
        //    stack.push(f.name);
        //    f = f.caller;
        //}
        //console.log(message, "from", stack);
        ////////////////////////////////////

        var logMessage = "window.onerror: " + msg + "\nurl: " + url + "\nline: " + line + "\ncol: " + colValue + "\nerrorValue: " + errorValue + "\nstack:\n" + stackValue;
        console.log(logMessage);

        // Dispatch as a WTF to mixpanel.
        var payload = {name: "WTF", properties: {msg: msg, url: url, line: line, col: colValue, error: errorValue, stackValue: stackValue} };
        App.dispatcher.dispatch(App.Actions.MIXPANEL_WTF, payload);

        var suppressErrorAlert = false;
        // If you return true, then error alerts (like in older versions of Internet Explorer) will be suppressed.
        return suppressErrorAlert;
    };

})(this);
