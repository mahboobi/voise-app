var GoogleAnalyticsStore = function () {
    var _this = this;  // Capture scope
    _this.googleInit = false;

    var _init = function () {
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-226182-33', 'auto');
        _this.googleInit = true;
    };

    var _track = function (payload) {
        if( _this.googleInit == false ) {
            _init();
        }
        // Google analytics looks as the window.location, so all we have to do is send a pageview!
        ga('send', 'pageview');
    };

    this.dispatchToken = App.dispatcher.register(function(action, payload) {
        if (action === App.Actions.TRACK_PAGE_VIEW){
            _track(payload);
        }
    });
};

GoogleAnalyticsStore.prototype = new AbstractStore();
