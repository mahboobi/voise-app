
var MixpanelStore = function () {

    // mixPanelId is unique for each web app instance. It will belong to the signed in user.
    var mixPanelId = null;
    var mixPanelWTFCtr = 0;
    var mixPanelWTFMAX = 10;

    var _setUserProfile = function (properties) {
        mixpanel.people.set(properties);
    };

    var _userContextUpdate = function (userContext){
        if (userContext.userOid){
            _identify(userContext.userOid)
        } else{
            _identify("anonymous");
        }
    };

    var _identify = function (unique_id) {
        mixpanel.identify(unique_id);
        mixpanel.wtfs.identify(unique_id);
    };

    var _track = function (payload) {
        try {
            // Mixpanel event tracking project is shared across desktop and web so we want to add from web property to all
            // events tracked by the web.
            // Add in the mixPanelId to all the tracking calls.
            if (payload.properties) {
                if (!payload.properties.from) {
                    payload.properties.from = "web";
                }
                payload.properties.mixPanelId = MixpanelStore.mixPanelId;
            } else {
                payload.properties = {from: "web", mixPanelId: MixpanelStore.mixPanelId};
            }
            payload.properties.webclient = App.Util.browserData.name + " v" + App.Util.browserData.version.full;
            mixpanel.track(payload.name, payload.properties);
        } catch(e){
            // if we fail to track, try reporting the fail
            _wtf({name:"error_while_event_tracking", properties:{stack_value: e.stack}});
        }
    };

    var _wtf = function (payload) {
        if (mixPanelWTFCtr < mixPanelWTFMAX) {
            mixPanelWTFCtr += 1;
            try {
                var webclient = App.Util.browserData.name + " v" + App.Util.browserData.version.full;
                if (payload.properties) {
                    payload.properties.webclient = webclient;
                } else {
                    properties = {webclient: webclient}
                }
                mixpanel.wtfs.track(payload.name, payload.properties)
            } catch (e) {
                // if we fail to track, try reporting the fail
                try {
                    mixpanel.wtfs.track("error_while_wtf_tracking", {stack_value: e.stack})
                } catch (e) {
                    // swallow because mixpanel failures shouldn't impact the user
                }
            }
        }
    };

    this.getMixPanelId = function () {
        // Note : the id is set to the existing cookie. If there is not a cookie, an id is created; this
        // allows tracking events that should be tied to sign-up, sign-in with an id that will identify
        // the BROWSER, to track repeated attempts to sign-in or up without a following completed event.
        if (!MixpanelStore.mixPanelId) {
            var mixPanelIdx = App.Util.getCookie("mixPanelId");
            if (!mixPanelIdx) {
                mixPanelIdx = 'NC-' + App.Util.getUUID();
            }
            MixpanelStore.mixPanelId = mixPanelIdx;
        }
        return MixpanelStore.mixPanelId;
    };

    this.setMixPanelId = function (usrMixPanelId) {
        MixpanelStore.mixPanelId = usrMixPanelId;
    };

    this.initialize = function () {
        this.getMixPanelId();
    };

    this.dispatchToken = App.dispatcher.register(function(action, payload) {
        if (action === App.Actions.MIXPANEL_TRACK){
            _track(payload);
        } else if (action === App.Actions.MIXPANEL_WTF){
            _wtf(payload);
        } else if (action === App.Actions.MIXPANEL_SET_USER_PROFILE) {
            _setUserProfile(payload);
        } else if (action === App.Actions.USER_CONTEXT_UPDATE){
            _userContextUpdate(payload)
        }
    });
};

MixpanelStore.prototype = new AbstractStore();
