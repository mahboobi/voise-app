var BrowserPropertyStore = function () {
    var windowResizeListener = null,
        windowWidth = $(window).width();
        windowHeight = $(window).height();

    this.getWindowHeight = function () {
        return windowHeight;
    };

    this.getWindowWidth = function () {
        return windowWidth;
    };

    this.widthClassFromWidth = function (pageWidth) {
        // Takes a window width (in pixels) and returns a class (mobile/small/medium/large)
        if (pageWidth < 475) {
            return "width-mobile";
        } else if (pageWidth >= 475 && pageWidth < 950) {
            return "width-small";
        } else if (pageWidth >= 950 && pageWidth < 1200) {
            return "width-medium";
        } else {
            return "width-large";
        }
    };

    this.subwidthClassFromWidth = function (pageWidth) {
        // Takes a window width (in pixels) and returns a sub class for more resolution
        if (pageWidth >= 475 && pageWidth < 600) {
            return "width-small-sub-1";
        } else if (pageWidth >= 600 && pageWidth < 775) {
            return "width-small-sub-2";
        } else if (pageWidth >= 775 && pageWidth < 950) {
            return "width-small-sub-3";
        } else {
            return "";
        }
    };

    this.heightClassFromHeight = function (pageHeight) {
        // Takes a window width (in pixels) and returns a class (mobile/small/medium/large)
        if (pageHeight < 350) {
            return "height-mobile";
        } else if (pageHeight >= 350 && pageHeight < 440) {
            return "height-small";
        }else if (pageHeight >= 440 && pageHeight < 600) {
            return "height-medium";
        } else if (pageHeight >= 600 && pageHeight < 800) {
            return "height-large";
        } else {
            return "height-max";
        }
    };

    this.getPlatform = function () {
        var platform = window.navigator.platform;
        if (platform.indexOf("iPhone")!=-1) return "ios";
        if (platform.indexOf("iPad")!=-1) return "ios";
        if (platform.indexOf("Win")!=-1) return "windows";
        if (platform.indexOf("Mac")!=-1) return "mac";
        if (platform.indexOf("X11")!=-1) return "unix";
        if (platform.indexOf("Linux")!=-1) return "linux";
        return "";
    };

    this.getGeneralPlatform = function () {
        var platform = window.navigator.platform;
        if (platform.indexOf("iPhone")!=-1) return "mac";
        if (platform.indexOf("iPad")!=-1) return "mac";
        if (platform.indexOf("Win")!=-1) return "windows";
        if (platform.indexOf("Mac")!=-1) return "mac";
        if (platform.indexOf("X11")!=-1) return "mac";
        if (platform.indexOf("Linux")!=-1) return "mac";
        return "";
    };

    this.setRenderingScale = function(renderingMaxHeight, renderingMaxWidth, topbarheight) {
        // Set the current rendering scale based upon an assumed full size rendering of a scaleMaxHeigh and scaleMaxWidth.
        // Values :
        //   width : current browser width
        //   height : current browser height
        //   widthScale : current ratio of the renderigMaxWidth to the browser width. Generally less then 1.0
        //   heightScale : current ratio of the renderigMaxHeight to the browser height. Generally less then 1.0
        //   autoScale : balance between the the widthScale and heightScale. This should be the rendering scale factor
        //               to apply to the AutoSizeBox/image,text for general scaling. It will take into account
        //               the profile or landscape orientation of the browser for generally scaling.
        //
        // Centering determinations:
        //      In the current app, the web broswer and gallery views are centered horizontally by dynamic padding
        //      in the left and right to set the center element - the drawn browser view - directly centered. This
        //      has worked very very well.   The new home page used that techinique along with the vertical centering
        //      of using the top padding to place the rendering 'box' directly into the center. This worked very well.
        //      These following items are the formalization of that technique to be used similiarly if desired.
        //
        // viewportBoxScaledHeight : the new height of the 'box' that is the renderingMaxHeight * renderingMaxWidth
        //               box / container passed in pixel units.  The new 'viewport' scaled per se.
        // viewportBoxScaledWidth : the new width of the 'box' that is the renderingMaxHeight * renderingMaxWidth
        //               box / container passed in pixel units. The new 'viewport' scaled per se.
        // leftWidthCentering : left padding % to center the above view port.
        // rightWidthCentering : right padding % to center the above view port.
        // topCenteringPixelOffset : top padding pixelse to center the above view port.

        var scaleMaxHeight = renderingMaxHeight || 850;
        var scaleMaxWidth = renderingMaxWidth || 1500;
        var topWindowOffset = topbarheight || 0;

        var renderingScale = {
            heightScale: 1.0,
            widthScale: 1.0,
            autoScale: 1.0,
            viewportBoxScaledHeight: scaleMaxHeight,
            viewportBoxScaledWidth: scaleMaxWidth,
            leftWidthCentering: 0,
            rightWidthCentering: 0,
            topCenteringPixelOffset: 0,
            width: windowWidth,
            height: windowHeight
        };

        var heightScale = windowHeight / scaleMaxHeight;
        if (heightScale > 1.0 ) {
            heightScale = 1.0;
        }

        var widthScale = windowWidth / scaleMaxWidth;
        if (widthScale > 1.0 ) {
            widthScale = 1.0;
        }

        var autoScale = heightScale;
        if (widthScale < autoScale) {
            autoScale = widthScale;
        }

        // Split the difference between the height scale and the width scale. This is a good compromise
        // to give the best rendering scaling to account for the default 'aspect' of the browser.
        var d1 = heightScale - widthScale;
        var r1 = d1 / 2; if (r1 < 0 ) {r1 = r1 * -1;}
        var autoScale = autoScale + r1;

        renderingScale.heightScale = heightScale;
        renderingScale.widthScale = widthScale;
        renderingScale.autoScale = autoScale;

        // Testing bad aspect ratio scaling
        var adjustedMaxWidthByHeightScale = heightScale * scaleMaxWidth;
        if (adjustedMaxWidthByHeightScale > windowWidth) {
            // alert("adjustedMaxWidthByHeightScale: " + adjustedMaxWidthByHeightScale);
            renderingScale.autoScale = renderingScale.widthScale;
        }

        // Set the scaled box width and height based on the given max viewport.
        if (heightScale > widthScale ) {
            renderingScale.viewportBoxScaledWidth = scaleMaxWidth * renderingScale.widthScale;
            renderingScale.viewportBoxScaledHeight = renderingScale.viewportBoxScaledWidth * (scaleMaxHeight / scaleMaxWidth);
            // alert("1 boxWidth: " + boxWidth + " boxHeight: " + boxHeight);
        } else {
            renderingScale.viewportBoxScaledHeight = scaleMaxHeight * renderingScale.heightScale;
            renderingScale.viewportBoxScaledWidth = renderingScale.viewportBoxScaledHeight * (scaleMaxWidth / scaleMaxHeight);
            // alert("2 boxWidth: " + boxWidth + " boxHeight: " + boxHeight);
        }

        // Set centering padding percent (15%) for the left and right.
        var centerWidth = renderingScale.viewportBoxScaledWidth;
        // Adjust values based upon the current width of the browser.
        if (renderingScale.width > centerWidth) {
            var viewportWidthRatio = centerWidth / renderingScale.width;
            var marginRemaining = 1.0 - viewportWidthRatio;
            var paddingWidth = (marginRemaining / 2) * 100;

            renderingScale.leftWidthCentering = paddingWidth;
            renderingScale.rightWidthCentering = paddingWidth;
        }

        // Set the centering height pixel offset (20px) from the top.
        var centerHeight = renderingScale.viewportBoxScaledHeight;
        // Adjust values based upon the current height of the browser.
        if (renderingScale.height > centerHeight) {
            var heightdiff = renderingScale.height - centerHeight;
            renderingScale.topCenteringPixelOffset = (heightdiff / 2) - topWindowOffset;
            // alert("heightdiff: " + heightdiff + " renderingScale.height: " + renderingScale.height + " - centerHeight: " + centerHeight + " topCenteringPixelOffset: " + renderingScale.topCenteringPixelOffset);
        }

        return renderingScale;
    };

    windowResizeListener = EventListener.listen(window, "resize", function() {
        windowWidth = $(window).width();
        windowHeight = $(window).height();
        this.emit();
    }.bind(this));

};

BrowserPropertyStore.prototype = new AbstractStore();
