/*
The FileStore provides access to file and folder metadata.

Externally, the FileStore presents a path-based interface with opaque identifiers. Users of public methods should
not attempt to split or join paths manually, instead they should use getParentPath and addComponentToPath to
traverse the store.

Internally, the FileStore's datastore is a nested structure. Each branch is identified by a filename. Since the
filenames can contain slashes, the store encodes/decodes them before they go in/out through its public methods.

To encode a path:
    1) Encode each component:
        a) Encode "%" to "%25"
        b) Encode "/" to "%2F"
    2) Join the components with a slash

To decode a path:
    1) Split on "/"
    2) Decode each component:
        a) Decode "%2F" and "%2f" to "/"
        b) Decode "%25" to "%"

Examples Path Encoding:
    - [] <-> "" (Root)
    - ["Facebook"] <-> "Facebook"
    - ["Facebook", "Weird/Name"] <-> "Facebook/Weird%2FName"
    - ["Facebook", "Weirder/Name%2F"] <-> "Facebook/Weirder%2FName%202F"
    - ["Facebook", "Weirder/Name%2F", "/.jpg"] <-> "Facebook/Weirder%2FName%202F/%2F.jpg"

Example datastore structure:

    dataStore = [
        name: "Facebook",
        sourceType: "Facebook",
        linkUri: "/Facebook",
        children: [
            {
                name: "Weird/Name",
                fileType: "FOLDER",
                linkUri: "/Facebook/239048329045823",
                children: [
                    {
                        name: "/.jpg",
                        fileType: "FILE"
                        linkUri: "/Facebook/23094832904536",
                    }
                ]
            }
        ]
    ]
*/

var WeblinkStore = function () {
    var dataStore = null;
    var weblinkPasswords = [];  // structure of each element: {passwordKey: 12345, password: abcd, expireEndSeconds: 1234}
    var _this = this;  // Capture scope

    // Private path manipulation methods

    function _encodePathComponent(component) {
        // Replace % with %25, then replace /, #, +, and ? with %2F, %23, %2B, and %3F respectively.
        return component.replace(/%/g, "%25")
                        .replace(/\//g, "%2F")
                        .replace(/#/g, "%23")
                        .replace(/\+/g,"%2B")
                        .replace(/\?/g, "%3F");
    }

    function _decodePathComponent(component) {
        // Replace %3F, %2B, %23, and 2F with ?, +, #, and / respectively, then replace %25 with %.
        return component.replace(/(%3F|%3f)/g, "?")
                        .replace(/(%2B|%2b)/g, "+")
                        .replace(/%23/g, "#")
                        .replace(/(%2F|%2f)/g, "/")
                        .replace(/%25/g, "%");
    }

    function _pathToComponents(path) {
        // Make sure there is exactly one leading slash and that there are no trailing slashes.
        var normalizedPath = path.replace(/(^\/*)|(\/*$)/g, '');
        // Special case for the root, we want to return an empty list.
        if (normalizedPath === "") {
            return [];
        }
        // Split the path on slashes and decode slashes within components.
        return normalizedPath.split("/").map(_decodePathComponent);
    }

    function _componentsToPath(pathComponents) {
        // Encode slashes in individual components, join them with slashes.
        return pathComponents.map(_encodePathComponent).join("/");
    }

    // Private datastore manipulation methods

    function _listFolder(children, pathComponents, maxItems) {
        var item;
        if (pathComponents.length === 0) {
            // Base case, we found the items.
            if (children) {
                return children.slice(0, maxItems);
            } else {
                return null;
            }
        } else {
            // Recurse down the tree
            item = _.find(children, function (item) {
                return item.disambiguatedName === pathComponents[0];
            });
            if (item && (item.sourceType || item.fileType === "FOLDER")) {
                return _listFolder(item.children, pathComponents.slice(1), maxItems);
            }
            else {
                return null;
            }
        }
    }

    function _getMetadata(pathComponents) {
        if (pathComponents.length === 0) {
            return {
                name: "My odrive",
                disambiguatedName: "My odrive",
                linkUri: '/',
                fileType: "FOLDER",
                path: ''
            }; // For the root, just return some placeholder info.
        }
        // List the parent, find the item with the matching name, and return it.
        var children = _listFolder(dataStore, pathComponents.slice(0, -1));
        var item = _.find(children, function (item) {
            return item.disambiguatedName === pathComponents[pathComponents.length - 1];
        });
        return item || null;
    }

    function _getSourceType(pathComponents) {
        if (pathComponents.length === 0) {
            return null;
        } else {
            return _getMetadata(pathComponents.slice(0, 1)).sourceType;
        }
    }

    function _setChildren(pathComponents, children) {
        // Set the children of the folder specified by pathComponents.

        var oldChildren;
        var childNames;
        var childrenWithGrandchildren = null;
        var oldMetadata;

        // If children is given as null, no need to transfer child properties...
        if (children) {
            // If one of the new children previously had children of its own, make sure to keep them
            oldChildren = _listFolder(dataStore, pathComponents, Infinity);
            childNames = {};  // Use an object as a map to keep the set of names.
            childrenWithGrandchildren = children.map(function (child) {
                // Disambiguate the names of the children
                if (childNames.hasOwnProperty(child.name)) {
                    child.disambiguatedName = child.name + child.linkUri;
                } else {
                    child.disambiguatedName = child.name;
                }
                childNames[child.disambiguatedName] = 1;  // Store the disambiguated name in the name map.
                oldMetadata = _getMetadata(pathComponents.concat(child.disambiguatedName));
                if (oldMetadata && oldMetadata.linkUri == child.linkUri) {
                    // Take the old child's children and pageToken and move them to the new child.
                    // If the linkUris differ, it is a different folder and will have different child contents.
                    child.children = oldMetadata.children;
                    child.pageToken = oldMetadata.pageToken;
                }
                return child;
            });
        }

        if (pathComponents.length === 0) {
            // We're updating the root, so just set the whole datastore to the new children.
            dataStore = childrenWithGrandchildren;
        } else {
            // Otherwise traverse down the tree and set the children of a specific node.
            _getMetadata(pathComponents).children = childrenWithGrandchildren;
        }
    }

    function _addChildren(pathComponents, children) {
        var oldChildren = _listFolder(dataStore, pathComponents, Infinity);
        var childNames = {};
        oldChildren.forEach(function (child) {
            childNames[child.disambiguatedName] = 1;
        });
        var disambiguatedChildren = oldChildren.concat(children.map(function (child) {
            if (childNames.hasOwnProperty(child.name)) {
                child.disambiguatedName = child.name + child.linkUri;
            } else {
                child.disambiguatedName = child.name;
            }
            childNames[child.disambiguatedName] = 1;
            return child;
        }));

        if (pathComponents.length === 0) {
            // We're updating the root, so just set the whole datastore to the new children.
            dataStore = childrenWithGrandchildren;
        } else {
            // Otherwise traverse down the tree and set the children of a specific node.
            _getMetadata(pathComponents).children = disambiguatedChildren;
        }
    }

    function _isRoot(pathComponents) {
        return pathComponents.length === 0;
    }

    function _isLinkRoot(pathComponents) {
        return pathComponents.length === 1;
    }

    function _getMore(pathComponents, password, success, error) {
        var metadata = _getMetadata(pathComponents);
        var pageToken = metadata.pageToken;
        var requestParameters = {
            "weblinkUri": metadata.linkUri,
            "pageToken": pageToken,
            "password": password
        };

        App.Util.ajax({
            url: "/rest/weblink/list_folder",
            data: requestParameters,
            dataType: 'json',
            success: function(response) {
                var existingMetadata = _getMetadata(pathComponents);
                if (!existingMetadata) {
                    // The parent path has been cleared from the store
                    if (success) {
                        setTimeout(success, 0);
                    }
                    return;
                }
                if (existingMetadata.pageToken !== pageToken) {
                    // If we got back a page but the page tokens mismatch, someone else already did this update for us.
                    if (success) {
                        setTimeout(success, 0);
                    }
                }

                var files = response.data.items || [];
                _addChildren(pathComponents, files);
                metadata.pageToken = response.data.nextPageToken;

                // If the dispatcher of this action specified a success callback, call it
                if (success) {
                    setTimeout(success, 0);
                }
                _this.emit();
            },
            error: function (errorCode, messageCode) {
                setTimeout(function () {
                    error(errorCode, messageCode);
                }, 0)
            }
        });
    }

    function _remoteUpdate(pathComponents, password, success, error) {
        var metadata;
        var requestParameters;

        metadata = _getMetadata(pathComponents);
        if (!metadata) {
            setTimeout(function () {
                error(App.Errors.INVALID_LINK_URI);
            }, 0);
            return;
        }

        App.Util.ajax({
            url: "/rest/weblink/list_folder",
            data: {
                weblinkUri: metadata.linkUri,
                password: password
            },
            dataType: 'json',
            success: function(response) {
                if (!_getMetadata(pathComponents)) {
                    // The parent path has been cleared from the store
                    if (success) {
                        setTimeout(success, 0);
                    }
                    return;
                }
                var files = response.data.items || [];
                _setChildren(pathComponents, files);
                metadata.pageToken = response.data.nextPageToken;

                // If the dispatcher of this action specified a success callback, call it
                if (success) {
                    setTimeout(success, 0);
                }
                _this.emit();
            },
            error: function (errorCode, messageCode) {
                setTimeout(function () {
                    error(errorCode, messageCode);
                }, 0);
            }
        });
    }

    function _setWeblinkPassword(password,passwordKey) {
        _removeWeblinkPassword(passwordKey);
        var currTime = new Date();
        var expireEndSeconds = currTime.getTime() + 60 * 60 * 1000; // 1 hour in milliseconds

        var passwordItem = {passwordKey: passwordKey, password: password, expireEndSeconds: expireEndSeconds};
        weblinkPasswords.push(passwordItem);
    }

    function _getWeblinkPassword(passwordKey) {
        var passwordItem = _.find(weblinkPasswords, function (item) {
            return item.passwordKey === passwordKey;
        });
        if (passwordItem) {
            var currTime = new Date();
            var remainingPasswordTime = passwordItem.expireEndSeconds - currTime;
            if (remainingPasswordTime > 0 ) {
                return passwordItem.password;
            } else {
                _removeWeblinkPassword(passwordKey);
                return null;
            }
        } else {
            return null;
        }
    }

    function _removeWeblinkPassword(passwordKey) {
        var match = -1;
        for (var i = 0; match == -1 && i < weblinkPasswords.length; ++i) {
            if (weblinkPasswords[i].passwordKey == passwordKey) {
                match = i;
            }
        }
        if (match != -1) {
            weblinkPasswords.splice(match,1);
        }
    }

    // Public methods.

    this.addComponentToPath = function (path, component) {
        var pathComponents = _pathToComponents(path);
        return _componentsToPath(pathComponents.concat(component));
    };

    this.getParentPath = function (path) {
        var pathComponents = _pathToComponents(path);
        if (pathComponents.length === 0) {
            return null;
        } else {
            return _componentsToPath(pathComponents.slice(0, -1));
        }
    };

    this.getWeblinkPath = function (path) {
        var pathComponents = _pathToComponents(path);
        if (pathComponents.length === 0) {
            return null;
        } else {
            return _componentsToPath(pathComponents.slice(0, 1));
        }
    };

    this.getFilename = function(path) {
        var pathComponents = _pathToComponents(path);
        return pathComponents.pop() || "My odrive";
    };

    this.getMetadata = function (path) {
        var pathComponents = _pathToComponents(path);
        return _getMetadata(pathComponents);
    };

    this.listFolder = function (path, maxItems) {
        var pathComponents = _pathToComponents(path);
        maxItems = maxItems || Infinity;
        return _listFolder(dataStore, pathComponents, maxItems);
    };

    this.hasMore = function(path) {
        var pathComponents = _pathToComponents(path);
        var metadata = _getMetadata(pathComponents);
        return !!(!metadata || !metadata.hasOwnProperty('pageToken') || metadata.pageToken);
    };

    this.isRoot = function (path) {
        return _isRoot(_pathToComponents(path));
    };

    this.isLinkRoot = function (path) {
        return _isLinkRoot(_pathToComponents(path));
    };

    this.getSourceType = function (path) {
        return _getSourceType(_pathToComponents(path));
    };

    this.dispatchToken = App.dispatcher.register(function(action, payload) {
        if (action === App.Actions.REMOTE_UPDATE_WEBLINK_FOLDER) {
            _remoteUpdate(_pathToComponents(payload.path), payload.password, payload.success, payload.error);
        } else if (action === App.Actions.GET_MORE_FOR_WEBLINK_FOLDER) {
            _getMore(_pathToComponents(payload.path), payload.password, payload.success, payload.error);
        } else if (action === App.Actions.SET_WEBLINK_PASSWORD) {
            _setWeblinkPassword(payload.password, payload.passwordKey);
        }
    });

    this.deleteMetadata = function (deleteItemPath, deleteItemLinkUri) {
        var parentFolder = this.getMetadata(this.getParentPath(deleteItemPath));
        if ( parentFolder.children ) {
            for (var i = 0; i < parentFolder.children.length; ++i) {
                if (parentFolder.children[i].linkUri === deleteItemLinkUri) {
                    parentFolder.children.splice(i, 1);
                    _this.emit();
                    break;
                }
            }
        }
    };

    this.updateMetadata = function (priorItemPath, priorLinkUri, updateItem) {
        // When updating the link, the link existing in the store is updated to the new updateItem properties it needs.
        // When updatign the folder, the actual folder item is replaced from the authoritative cloud updateItem, and
        // the locally modified items needed by the store are carried over.
        if (this.isLinkRoot(priorItemPath)) {
            var linkFolder = this.getMetadata(priorItemPath);
            linkFolder.name = updateItem.name;
            linkFolder.disambiguatedName = updateItem.name;
            linkFolder.linkUri = updateItem.linkUri;
        } else {
            var parentFolder = this.getMetadata(this.getParentPath(priorItemPath));
            if (parentFolder.children) {
                for (var i = 0; i < parentFolder.children.length; ++i) {
                    if (parentFolder.children[i].linkUri === priorLinkUri) {
                        // Check for another file with the same name. If so, add the linkUri to the disambiguatedName.
                        // Otherwise, the disambiguatedName is the item name.
                        // When updating with a rename, if there is another file/folder with the same name that has not
                        // been paged in, it will have the disambiguatedName resolved correctly when it is finally
                        // paged to.
                        var duplicateItem = _.find(parentFolder.children, function(item) {
                            return item.name === updateItem.name;
                        });

                        if (duplicateItem) {
                            updateItem.disambiguatedName = _encodePathComponent(updateItem.name + updateItem.linkUri);
                        } else {
                            updateItem.disambiguatedName = updateItem.name;
                        }

                        // Update the other client created fields for the object.
                        updateItem.children = parentFolder.children[i].children;
                        updateItem.pageToken = parentFolder.children[i].pageToken;

                        parentFolder.children[i] = updateItem;
                        _this.emit();
                        break;
                    }
                }
            }
        }
    };

    this.addChildren = function (parentPath, childFiles) {
        var pathComponents = _pathToComponents(parentPath);
        _addChildren(pathComponents, childFiles);
        _this.emit();
    };

    this.setChildren = function (parentPath, childFiles) {
        var pathComponents = _pathToComponents(parentPath);
        _setChildren(pathComponents, childFiles);
        _this.emit();
    }

    this.getWeblinkPassword = function (passwordKey) {
        return _getWeblinkPassword(passwordKey);
    }
};

WeblinkStore.prototype = new AbstractStore();
