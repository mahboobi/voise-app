Number.prototype.times = function(callback) {
	var i,
	    result = [];
	for (i = 0; i < this; i++) {
		result.push(callback(i));
	}
	return result;
};
