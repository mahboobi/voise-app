var AbstractStore = function () {
	var eventEmitter = new EventEmitter();

    this.addChangeListener = function (callback) {
        eventEmitter.addListener('change', callback);
        return {
            remove: function () {
                eventEmitter.removeListener('change', callback);
            }
        };
    };

    this.emit = function () {
        // emit event async
        setTimeout( function() {
            eventEmitter.emitEvent('change');
        }, 0);
    };
};